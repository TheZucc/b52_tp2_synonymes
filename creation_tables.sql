DROP TABLE cooccurrences CASCADE CONSTRAINTS;
DROP TABLE dictionnaire CASCADE CONSTRAINTS;

CREATE TABLE dictionnaire(
	id    NUMBER(6)   NOT NULL,
	mot  VARCHAR2(64) UNIQUE,
	PRIMARY KEY(id)
);
CREATE TABLE cooccurrences(
  id    NUMBER(10)   NOT NULL,
  mot_1 number(6),
  mot_2 number(6),
  fenetre_cooc number(2),
  nb_cooccurrences NUMBER(6) DEFAULT 0,
  
  CONSTRAINT pk_cooccurences PRIMARY KEY(id), 
  
  CONSTRAINT fk_mot_1
  FOREIGN KEY (mot_1)
  REFERENCES dictionnaire(id),
  
  CONSTRAINT fk_mot_2
  FOREIGN KEY (mot_2)
  REFERENCES dictionnaire(id)
);


