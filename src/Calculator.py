#-*- coding: utf8 -*-
#===============================================================================================
# Fichier : Calculator.py
# Projet  : B52_TP2
# Auteurs : Kevin Mwanangwa, Laurier Lavoie-Giasson, Chris David
#===============================================================================================

#Imports =======================================================================================
import numpy as np

#Class Calculator ==============================================================================
class Calculator():
    def __init__(self,parser,connexion,fenetre):
        #Parser
        self.parser = parser
        #Connexion SQL
        self.connexion = connexion
        #Taille Fenetre
        self.fenetre = fenetre
        # Taille du dictionnaire    
        self.tailleVoc = len(self.connexion.dictionnaire)            
        # Matrice de calcul
        self.matrice = np.zeros((self.tailleVoc,self.tailleVoc)) 
        self.stoplist = self.loadStopList()  
        #coocs a inserer
        self.coocs = {}
      
    #Charger la stop-liste --------------------------------------------------------------------
    def loadStopList(self):
        #Ouvrir liste
        fichier = "TP2_KevLauChr_StopList.txt"
        stream = open(fichier,"r", encoding="utf-8")
        liste = stream.read().replace('\ufeff','').split() 
        stream.close()
                
        indexes=[]
        #Verifier chaque mots du dictionnaire
        for (mot,idx) in self.connexion.dictionnaire.items():
            #Si mot actuel est dans la stop-liste
            if mot in liste:
                #Ajouter index du mot a la liste d'indexes
                indexes.append(idx+1)
                                  
        return indexes        
    #Changer les variables pour le calcul------------------------------------------------------
   
    def updateValues(self,motVoulu,nbSyns,fonction):
        self.fonction = fonction
        self.motVoulu = motVoulu
        self.nbSyn = nbSyns        
       
    def calculer_coocurences(self):
        for phrase in self.parser.corpus:
            #Index de la phrase dans le corpus
            indexPhrase = self.parser.corpus.index(phrase) 
                              
            #Variables (debut, milieu, fin )
            d = 0 #              
            m = 0
            f = int(self.fenetre/2) 
            #Index du mot actuel dans la phrase
            indexActuel = 0  
            
            #Tant que f est plus petit que la longueur de la phrase
            while indexActuel < len(phrase):      
                #Si debut de phrase, dont m pas encore arrive dans le milieu de la fenetre           
                if indexActuel <= int(self.fenetre/2):      
                    #Tronquer le debut                 
                    d = 0        
                #Si deuxieme moitie de la fenetre depasse des limites de la phrase
                if indexActuel > (len(phrase)-1)-int(self.fenetre/2):
                    #Tronquer la fin
                    f = len(phrase)-1
                
                #Reset le debut de la fenetre          
                i = d                                
                #Tant que le curseur i n'est pas passÃ© Ã  travers chaque mot de la fenetre                 
                while i <= f:                                
                    #S'assurer de ne pas comparer le mot milieu Ã  lui meme 
                    if i != m:        
                        #Trouver le mot milieu actuel dans le corpus   
                        motCorpus = self.parser.corpus[indexPhrase][m]
                        #Trouver l'index du mot milieu actuel dans le dictionnaire
                        indexMot = self.connexion.dictionnaire[motCorpus] 
                        #Trouver le mot coocurent actuel dans le corpus
                        motCooc = self.parser.corpus[indexPhrase][i] 
                        #Trouver l'index du mot coocurent actuel dans le dictionnaire
                        indexCooc = self.connexion.dictionnaire[motCooc] #Index du mot a checker
                        #Incrementer la valeur dans la matrice                        
                        #self.matrice[indexMot][indexCooc]+=1
                        
                        #
                        #
                        #
                        #
                        #pour régler l'erreur que nous essayons de régler depuis 8 heures
                        #
                        #
                        #
                        #   
                        if len(self.connexion.coocs) == 0:                    
                            tmp = (indexMot+1,indexCooc+1)
                        else:
                            tmp = (indexMot,indexCooc)
                        #
                        #
                        #
                        #
                        #
                        #
                        #
                        #    
                        if tmp in self.coocs:
                            self.coocs[tmp]+=1
                        else:
                            self.coocs[tmp] = 1          
                    #Changer la position du curseur                                                        
                    i+=1
                #Passer Ã  la fenetre/au mot suivant    
                d+=1
                m+=1
                f+=1
                indexActuel+=1

    def remplir_matrice(self):      
        for ((mot1,mot2),idx) in self.connexion.coocs.items():
            self.matrice[mot1-1][mot2-1] = self.connexion.nbcoocs[idx]  
          
    def score(self):
        #Index du mot voulu
        indexMot = self.connexion.dictionnaire[self.motVoulu] -1
             
        #Compteur
        i=0        
        #Dictionnaire des (index,scores)
        scores = {}
        #Vecteur du motVoulu
        vecA=self.matrice[indexMot]       
         
        #Comparer vecA a tout les autre vecteurs de la matrice
        while i < self.tailleVoc-1:
            #Ne par comparer vecA a lui meme
            if i != indexMot:
                #Verifier que le mot actuel n'est pas dans la stop-liste
                if i not in self.stoplist:
                    #Prendre matrice a comparer en tampon
                    vecB = self.matrice[i]                 
                    #Faire operation
                    if self.fonction == 0:
                        result=np.inner(vecA,vecB)
                    elif self.fonction == 1:
                        result=np.sum(np.square(vecA-vecB))
                    else:                        
                        result=np.sum(np.absolute(vecA-vecB))  
                    #Ajouter le score 
                    scores[i]=result                                       
            i+=1               
             
        #Si Produit Scalaire, retourner liste decroissante        
        if self.fonction == 0:
            return self.sort_and_display(-1,scores)
        #Sinon, retourner liste croissante
        else:
            return self.sort_and_display(1,scores)
    
    def sort_and_display(self,ordre,scores):        
        #Listes temporaires   
        tmpScores = [] 
        tmpIndexes = [] 
        for (index,score) in scores.items():        
            tmpScores.append(score)           
            tmpIndexes.append(index) 
        
        #Trier les plus hauts resultats en ordres descroissant et ne garder que le nombre de synonymes voulus        
        highest = np.argsort(tmpScores)[::ordre][:self.nbSyn] #Retourne les indexes des scores en ordres
               
        
        #Afficher la liste 
        #Taille/longueur de la string la plus longue des meilleur resultats
        longestScore = len(str(tmpScores[highest[0]])) 
        #Si dernier score plus long que premier (selon la fonction), changer valeur
        if len(str(tmpScores[highest[self.nbSyn-1]])) > longestScore:
            longestScore = len(str(tmpScores[highest[self.nbSyn-1]]))
       
        #Titre 
        strTitre = "<TOP "+str(self.nbSyn)+" SYNONYMES POUR LE MOT : "+self.motVoulu+">"        
        print("\n"+strTitre)
        
        #Tabulations           
        tabMot = len(str(self.nbSyn))+2       
        tabScore = 30
        largeur = tabMot + tabScore + longestScore + (10-tabMot)
        
        #maxRow
        maxRow="="*largeur 
        #emptyRow
        emptyRow="|"+(" "*(largeur-2))+"|"     
        
        #Compteur
        i=0
        print(maxRow) 
        print(emptyRow)
        for i in range(self.nbSyn):
            idxScore = highest[i]            
            indexMot = tmpIndexes[idxScore]
            #Trouver mot correspondant a l'index et afficher (Mot,Score)                      
            for (mot,idx) in self.connexion.dictionnaire.items():
                if indexMot == idx-1: 
                    #String temporaire, tab mot, mot
                    tmpStr = ((("| "+str(i+1)).ljust(tabMot)+".) "+mot).ljust(tabScore)+"Score : "+str(tmpScores[highest[i]])).ljust(largeur-1)+"|"
                    print(tmpStr)
                    print(emptyRow)                   
                    
            i+=1    
        print(maxRow)

