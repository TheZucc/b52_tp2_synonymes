#-*- coding: utf8 -*-
#===============================================================================================
# Fichier : Main.py
# Projet  : B52_TP2
# Auteurs : Kevin Mwanangwa, Laurier Lavoie-Giasson, Chris David
#===============================================================================================

#Imports =======================================================================================
import sys
from pip._vendor.distlib.compat import raw_input
from TextParser import TextParser
from SQLConnector import SQLConnector
import time
from Calculator import Calculator

inputUsr = None

def verifyMainCall():
    #INPUT VIDE --------------------------------------------------------------------------------
    if len(sys.argv) < 2:
        print("/!\: Aucun paramètres, veuillez entrer des paramètres.")
    elif len(sys.argv) == 2:
        if sys.argv[1] == "mots_svp":
            connexion = SQLConnector();
            resultats_requetes = connexion.selectionner_tous_mots()
            for mot in resultats_requetes["liste_mots"]:
                print(mot)
            print("le dictionnaire comporte " + str(resultats_requetes["nb_mots"]) + " mots.")
            return False
    #TACHE INVALIDE ----------------------------------------------------------------------------
    elif(sys.argv[1] != "-s" and sys.argv[1] != "-e"):
        print("/!\: Paramètre invalide, ('"+sys.argv[1]+"') n'est pas une tâche valide (-e ou -s).")   
    elif len(sys.argv) == 2:
        print("/!\: Paramètre manquant, paramètre de taille manquant ('-t').")    
    #TAILLE MANQUANTE ---------------------------------------------------------------------------    
    elif len(sys.argv)>=3 and sys.argv[2] != "-t":               
        print("/!\: Paramètre manquant, paramètre de taille manquant ('-t'), paramètre ('"+sys.argv[2]+"') invalide.")
    elif len(sys.argv) == 3:
        print("/!\: Paramètre manquant, veuillez entrer une taille pour la fenêtre.")
    #TAILLE INVALIDE ----------------------------------------------------------------------------
    elif len(sys.argv)>3:               
        try:
            int(sys.argv[3])            
        #En cas de valeur non-numérique
        except ValueError:
            print("/!\: Paramètre invalide, ('"+sys.argv[3]+"') n'est pas une valeur numérique.") 
            return False            
        #RECHERCHE INVALIDE -------------------------------------------------------------------------
        if len(sys.argv) == 4:
            #En cas de mauvaise tache
            if sys.argv[1] == "-e":
                print("/!\: Paramètres manquants, trop peu de paramètres pour un entrainement (-enc, -cc).")
            #RECHECHE VALIDE ------------------------------------------------------------------------
            else:
                return True                
        #ENTRAINEMENT --------------------------------------------------------------------------------
        elif len(sys.argv) == 8:
            #En cas de mauvaise tache
            if sys.argv[1] == "-s":
                print("/!\: Paramètres invalides, trop de paramètres pour une recherche.")
            else:
                if sys.argv[4] != "-enc":
                    print("/!\: Paramètre invalide, paramètre ('-enc') manquant, paramètre ('"+sys.argv[4]+"') invalide.")
                elif sys.argv[6] != "-cc":
                    print("/!\: Paramètre invalide, paramètre ('-cc') manquant, paramètre ('"+sys.argv[4]+"') invalide.")
                else:
                    return True
        else:
            print("/!\ Nombre de paramètres invalide")
    
    #IL RESTE A VOIR LE ERREUR DE CHEMIN INVALIDE ET D'ENCODAGE INVALIDE !!!!!!!!!!!!!!!!!       
    return False

def verifySearchCall():
    # Deuxieme Input 
    print("\nEntrez un mot, le nombre de synonymes que vous voulez et la methode de calcul:\n ( Produit Scalaire: 0, Least Squares: 1, CityBlock: 2 )\n\nTapez \"-1\" pour quitter\n\n")  
    inputUsr = raw_input("Selection: ").split() 
    valide = False
    
    #VERIFIER LA VALIDITE DE L'INPUT OU QUITTER SI -1
    while str(inputUsr[0]) != "-1" and valide == False: 
        #SI MAUVAIS NOMBRE D'ARGUMENTS ------------------------------------------------------------------------
        if len(inputUsr) < 3:
            print("\n /!\: Vous n'avez pas entre assez d'arguments.")            
        elif len(inputUsr) > 3:
                print("\n /!\: Vous avez entre trop d'arguments.")                
        #SI BON NOMBRE D'ARGUMENTS ----------------------------------------------------------------------------
        else:                
            #VERIFIER QUE LE MOT EST DANS LE DICTIONNAIRE
            isInDict = True
            if (isInDict):    
                #ERREURS DE DEUXIEME ARGUMENTS --------------------------------------------------------------------
                isNumeric = False
                try:                
                    x = int(inputUsr[1])
                    isNumeric = True
                except ValueError:
                        print("\n /!\: Nombre de synonymes invalide, ('"+inputUsr[1]+"') n'est pas une valeur numérique.")
                #ERREURS DE TROISIEME ARGUMENTS ----------------------------------------------------------------
                if(isNumeric): 
                    try:                
                        x = int(inputUsr[2])
                        if x > 2 or x < 0:
                            print("\n /!\: ('"+inputUsr[2]+"') n'est pas une méthode de calcul valide.")                                
                        else:
                            #TOUT EST BON
                            return (True,inputUsr[0],int(inputUsr[1]),int(inputUsr[2])) 
                    except ValueError:
                        print("\n /!\: Methode de calcul invalide, ('"+inputUsr[2]+"') n'est pas une valeur numérique.")                            
             
        #RESET, SI ON ARRIVE ICI, INPUT EST TOUJOURS INVALIDE
        print("\nEntrez un mot, le nombre de synonymes que vous voulez et la methode de calcul:\n ( Produit Scalaire: 0, Least Squares: 1, CityBlock: 2 )\n\nTapez \"-1\" pour quitter\n\n")  
        inputUsr = raw_input("Selection: ").split() 
    #SI ON ARRIVE ICI, ON A ECRIT -1
    return (False,)       
        
#Fonction Main =================================================================================

def main():  
    #SI LIGNE DE COMMANDE VALIDE EXECUTER, SINON QUIT
    if(verifyMainCall()):
        fenetre = int(sys.argv[3])
        #CONNEXION ----------------------------------------------------------                                         
        connexion = SQLConnector(fenetre)
            
        #Entrainement -----------------------------------------------------------
        if sys.argv[1] == "-e":
            #Parametres 
            encodage = sys.argv[5]
            chemin = sys.argv[7]
            
            #PARSING ------------------------------------------------------------   
            print("   /PROGRAM/  Parser texte")         
            startparse = time.time()                          
            parser = TextParser(connexion,chemin,encodage)            
            parser.parser()
            endparse = time.time()-startparse
            print("     /TIMER/  Parser les",parser.nbMots," mots du texte "+chemin+" : ",endparse, "secondes")
                       
            #INSERTION DICTIONNAIRE ---------------------------------------------
            print("   /PROGRAM/  Inserer dictionnaire dans BD")
            startmany = time.time() #timer
            connexion.insertion_mots(parser.corpus)
            endmany = time.time()-startmany
            print("     /TIMER/  Insertion des nouveaux mots dans la table DICTIONNAIRE : ",endmany, "secondes")    
            
            #CALCULS COOCCURRENCES ----------------------------------------------
            print("   /PROGRAM/  Calculer coocurrences")
            startMatrix = time.time()
            calculator = Calculator(parser,connexion,fenetre)
            calculator.calculer_coocurences()
            endMatrix = time.time()-startMatrix            
            print("     /TIMER/  Calculs des coocurences : ",endMatrix, "secondes")    
            
            #INSERTION COOCCURRENCES --------------------------------------------
            print("   /PROGRAM/  Inserer cooccurrences dans la BD")
            startInsert = time.time()
            connexion.insertion_coocurrences(calculator.coocs)
            endInsert = time.time()-startInsert
            print("     /TIMER/  Insertions/Updates de la table COOCCURRENCES : ",endInsert, "secondes")    
            
            #Temporaire
            #connexion.testCOOCS()
            connexion.cur.close()
            connexion.connexion.close()
                   

        #Recherche --------------------------------------------------------
        else:
           
            calculator = Calculator(None,connexion,fenetre)
            #REMPLIR LA MATRICE AVEC LES COOCS
            print("   /PROGRAM/  Remplir matrice interne")
            startRemplir = time.time()
            calculator.remplir_matrice()
            endRemplir = time.time() - startRemplir
            print("     /TIMER/  Remplir la matrice avec infos de la table COOCCURRENCES :",endRemplir, "secondes")  
                        
            tmp = verifySearchCall()
            while(tmp[0] == True):          
                motVoulu = tmp[1]
                nbSyns = tmp[2]
                fonction = tmp[3]
                calculator.updateValues(motVoulu, nbSyns, fonction) 
                #CALCULER LES SCORES ET AFFICHER
                startScore = time.time()
                print("   /PROGRAM/  Debut calculs de synonymes")
                calculator.score()     
                endScore = time.time() - startScore
                print("        /TIMER/  Calculs faits en : ",endScore, "secondes")  
                tmp = verifySearchCall()           
            #Si l'usager veut quitter
            else:       
                return 0   
    else:            
        return 0


#Appel Fonction Main ===========================================================================
if __name__ == '__main__':
    sys.exit(main());