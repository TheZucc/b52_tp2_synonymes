#-*- coding: utf8 -*-
#===============================================================================================
# Fichier : SQLConnector.py
# Projet  : B52_TP2
# Auteurs : Kevin Mwanangwa, Laurier Lavoie-Giasson, Chris David
#===============================================================================================

#Imports =======================================================================================
import sys
import cx_Oracle
import time

#Class SQLConnector ============================================================================
class SQLConnector():
    def __init__ (self,fenetre):
        #Connection a la BD
        print("   /PROGRAM/  Debut connection")
        startConn = time.time()
        PATH_ORACLE = 'C:\Oracle\Client\product\12.2.0\client_1\bin'
        sys.path.append(PATH_ORACLE)    
        dsn_tns = cx_Oracle.makedsn('delta', 1521, 'decinfo')
        chaineConnexion = 'e1484242' + '/' + "ZZZzzz111"+ '@' + dsn_tns
        self.connexion = cx_Oracle.connect(chaineConnexion)
        self.cur = self.connexion.cursor()
        endConn = time.time() - startConn
        print("     /TIMER/  Connection avec la BD : ",endConn, "secondes")        
        
        #Parametres
        self.fenetre = fenetre
                
        #Dictionnaire qui va contenir le dictionnaire de la BD
        self.dictionnaire = {}
        #Init le dictionnaire
        print("   /PROGRAM/  Remplir dictionnaire interne")
        startDict = time.time()
        self.get_dict()  
        endDict = time.time() - startDict
        print("     /TIMER/  Recuperer ",len(self.dictionnaire)," lignes dans la table DICTIONNAIRE : ",endDict, "secondes")   
        
        #Dictionnaire qui va contenir les coocurrences (mot_1,mot_2)
        self.coocs = {}
        #Dictionnaire qui va contenir les coocurrences (mot_1,mot_2)
        self.nbcoocs= {}
        #Init le nombre de coocs precedentes
        print("   /PROGRAM/  Remplir cooccurrences internes")
        startCoocs = time.time()
        self.get_coocs()  
        endCoocs = time.time() - startCoocs
        print("     /TIMER/  Recuperer ",len(self.coocs)," lignes dans la table COOCCURRENCES : ",endCoocs, "secondes")  
        
        #Sequences internes             
        self.currvalDict()        
        self.currvalCooc()
    
    #Valeur actuelle de la sequence interne    
    def currvalDict(self):
        #Recupere le nombre d'elements dans la table DICTIONNAIRE
        enonce = "SELECT count(*) FROM dictionnaire "
        self.cur.execute(enonce)
        for rangee in self.cur.fetchall(): 
                self.idDict = int(rangee[0])
       
    #Prochaine valeur de la sequence interne            
    def nextvalDict(self):        
        #Incrémente la valeur       
        self.idDict += 1
        return self.idDict
    
    #Valeur actuelle de la sequence interne     
    def currvalCooc(self):
        #Recupere le nombre d'elements dans la table DICTIONNAIRE
        enonce = "SELECT count(*) FROM cooccurrences "
        self.cur.execute(enonce)
        for rangee in self.cur.fetchall():            
            self.idCooc = int(rangee[0]) 
    
    #Prochaine valeur de la sequence interne            
    def nextvalCooc(self): 
        #Incrémente la valeur       
        self.idCooc += 1
        return self.idCooc
    
    #Cherche la table DICTIONNAIRE de la BD   
    def get_dict(self):
        enonce = "SELECT * FROM dictionnaire"        
        self.cur.execute(enonce)
        for rangee in self.cur.fetchall():    
            #Inserer (mot,id)         
            self.dictionnaire[rangee[1]]=int(rangee[0])   
     
    #Cherche la table COOCCURRENCES de la BD       
    def get_coocs(self):
        enonce = "SELECT * FROM cooccurrences WHERE fenetre_cooc = "+str(self.fenetre)+" ORDER BY id"        
        self.cur.execute(enonce)
        for ligne in self.cur.fetchall():           
            #Inserer mot1, mot2 et nbCooccurrences
            idmot1 = int(ligne[1])
            idmot2 = int(ligne[2])
            idx = int(ligne[0])
            nbc = int(ligne[4])
            self.coocs[(idmot1,idmot2)] = idx
            self.nbcoocs[idx] = nbc        
            
                
    #Inserer les nouveaux mots dans la BD   
    def insertion_mots(self, corpus):
        enonceBatch = "INSERT INTO dictionnaire(id,mot) VALUES(:1,:2)"
        params=[]               
        
        #Toutes les phrases
        for phrase in corpus:
            #Tout les mots dans la phrase
            for mot in phrase:
                #Si le mot n'est pas deja dans le dictionnaire, ajouter
                if mot not in self.dictionnaire:                    
                    index = self.idDict
                    self.dictionnaire[mot] = index
                    tup = (self.nextvalDict(),mot)
                    params.append(tup)
                    
       
        #Si il y a au moins un mot a inserer (eviter les insertions vides)
        if len(params)>0:           
            self.cur.executemany(enonceBatch, params, batcherrors=True)
            self.connexion.commit()            
        return 0  
    
    def insertion_coocurrences(self,coocs):       
        paramsInsert = []
        paramsUpdate = []
       
        #Chacune des coocurences        
        for (coocActuelle,nbcoocs) in coocs.items():            
            #SI COOC ACTUELLE ABSENTE DE LA BD, INSERT 
            if coocActuelle not in self.coocs:                 
                tup = (self.nextvalCooc(),)+coocActuelle+(self.fenetre,nbcoocs)                 
                paramsInsert.append(tup)                
            #SINON UPDATE COOC EXISTANTE
            else:
                tup = (nbcoocs,self.coocs[coocActuelle])
                paramsUpdate.append(tup)
                
        #Si il y a au moins un mot a inserer (eviter les insertions vides)
        if len(paramsInsert)>0:
            print("   /PROGRAM/ NB D'INSERTIONS : ",len(paramsInsert))
            enonceInsert = "INSERT INTO cooccurrences(id,mot_1,mot_2,fenetre_cooc,nb_cooccurrences) VALUES(:1,:2,:3,:4,:5)"
            self.cur.executemany(enonceInsert, paramsInsert, batcherrors=True)  
            self.connexion.commit() 
            print("   /PROGRAM/ FIN INSERTIONS")
        #Si il y a au moins un mot a update (eviter les updates vides)              
        if len(paramsUpdate)>0:     
            print("    /PROGRAM/ NB D'UPDATES : ",len(paramsUpdate))     
            enonceUpdate = "UPDATE cooccurrences SET nb_cooccurrences = (nb_cooccurrences + :1) WHERE id = :2"
            self.cur.executemany(enonceUpdate, paramsUpdate, batcherrors=True)
            self.connexion.commit() 
            print("    /PROGRAM/ FIN UPDATES")
         
    
    def selectionner_tous_mots(self):
        returnval = {}
        liste_mots = []
        nb_mots = 0
        enonce = "SELECT mot FROM dictionnaire"
        self.cur.execute(enonce)
        for rangee in self.cur.fetchall():
            for cell in rangee:
                liste_mots.append(str(cell))
        returnval["liste_mots"] = liste_mots
        nb_mots = len(liste_mots)
        returnval["nb_mots"] = nb_mots
        return returnval