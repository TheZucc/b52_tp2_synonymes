#-*- coding: utf8 -*-
#===============================================================================================
# Fichier : TextParser.py
# Projet  : B52_TP2
# Auteurs : Kevin Mwanangwa, Laurier Lavoie-Giasson, Chris David
#===============================================================================================

#Imports =======================================================================================
import re

#Class TestParser ============================================================================
class TextParser():
    def __init__(self,connexion,path,enc):
        # Fichier a parser
        self.fichier = path       
        # Encodage de lecture:
        self.encodage = enc
        # Ensemble des mots en ordre:
        self.corpus=[] 
        # Caracteres a enlever:
        self.ponctuation = "\"'#$%&()*+,/.:;<=->?@[\]^_`{|}\~\ufeff"
     
        self.aEnlever = str.maketrans(self.ponctuation,' '*len(self.ponctuation))
        self.nbMots = 0
      
    #Parser le texte ---------------------------------------------------------------------------
    def parser(self):            
        # Ouvrir le fichier
        stream = open(self.fichier,"r", encoding=self.encodage)
        texte = stream.read()   
        # Diviser le texte en phrases( En enlevant les ponctuations de fin de phrase )
        regex = re.compile(r"[.!?] ")        
        phrases = re.split(regex,texte) 
        # Traiter chaque phrase      
        for phrase in phrases: 
            #Remplacer les pontuactions par des WHITESPACES et split les mots resultants    
            mots = (phrase.lower().translate(self.aEnlever)).split()      
            self.corpus.append(mots)
            self.nbMots += len(mots)            
        # Fermer le stream    
        stream.close()
            
    
    